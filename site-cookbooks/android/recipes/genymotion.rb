
genymotion_ver = "genymotion-#{node[:genymotion][:version]}"
genymotion_bin = "#{Chef::Config[:file_cache_path]}/#{genymotion_ver}.bin"

execute 'install_genymotion' do
  command "echo 'Y' | #{genymotion_bin} -d #{node[:genymotion][:install_path]}"
  action :nothing
end

remote_file genymotion_bin do
  source "http://files2.genymotion.com/genymotion/#{genymotion_ver}/#{genymotion_ver}_x64.bin"
  action :create_if_missing
  owner node[:genymotion][:owner]
  group node[:genymotion][:group]
  mode 0755
  notifies :run, 'execute[install_genymotion]', :immediately
end
