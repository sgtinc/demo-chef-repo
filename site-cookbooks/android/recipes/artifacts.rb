directory node[:artifacts][:install_path] do
  owner node[:artifacts][:owner]
  group node[:artifacts][:group]
  action :create
end

ruby_block 'download_artifacts' do
  block do
    require 'net/http'
    require 'uri'
    require 'json'

    user_id = Etc.getpwnam(node[:artifacts][:owner]).uid
    group_id = Etc.getgrnam(node[:artifacts][:group]).gid

    path = "#{node[:jenkins][:url]}job/#{node[:jenkins][:job_path]}/#{node[:jenkins][:job_num]}"
    uri = URI.parse("#{path}/api/json?tree=artifacts[relativePath,fileName]")

    artifacts = nil
    Net::HTTP.start(uri.host, uri.port) do |http|
      request = Net::HTTP::Get.new uri.request_uri
      request.basic_auth node[:jenkins][:user], node[:jenkins][:api_key]

      response = http.request request

      artifacts = JSON.parse(response.body)['artifacts']
    end

    artifacts.each { |artifact|
      Net::HTTP.start(uri.host, uri.port) do |http|
        dl_uri = URI.parse "#{path}/artifact/#{artifact['relativePath']}"
        request = Net::HTTP::Get.new dl_uri.request_uri
        request.basic_auth node[:jenkins][:user], node[:jenkins][:api_key]

        response = http.request request

        File.open "#{node[:artifacts][:install_path]}/#{artifact['fileName']}", 'wb' do |file|
          file.write response.body
          file.chown user_id, group_id
        end
      end
    }
  end
end

